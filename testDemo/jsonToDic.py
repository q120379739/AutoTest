import json
if __name__ == '__main__':
    json_str= '{"姓名":"name","班級":"class"}'
    # 原类型
    print("原类型：", type(json_str))

    # 转成dict对象
    # 会被转换成字典类型
    json_dict = json.loads(json_str)
    print("转换后的类型：", type(json_dict))

    # 遍历字典
    for (k, v) in json_dict.items():
        print(k, " : ", v)
    # -----------
    print("dict转换为json")
    dict1={
        "姓名": "name", "班級": "class"
    }
    print("原类型:",type(dict1))
    json1=json.dumps(dict1)
    print("转换后:",type(json1))
    print(json1)
