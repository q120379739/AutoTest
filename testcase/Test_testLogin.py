import json
import os
import allure
import time
import pytest
import requests

from common import ExcelConfig, Base
from config import Conf
from config.Conf import ConfigYaml
from utils.LogUtil import my_log
from utils.MysqlUtil import Mysql
from utils.RequestsUtil import Request
from  utils.AssertUtil import AssertUtil
from common.ExcelData import Data
from common.Base import init_db

#1、初始化信息
#1）.初始化测试用例文件
case_file = os.path.join(Conf.get_data_path(),ConfigYaml().get_excel_file())
#2）.测试用例sheet名称
sheet_name = ConfigYaml().get_excel_sheet()
#3）.获取运行测试用例列表
data_init = Data(case_file,sheet_name)
run_list = data_init.get_run_data()

#4）.日志
log = my_log()
#初始化dataconfig
data_key = ExcelConfig.DataConfig


class TestLogin:

    @pytest.mark.parametrize("case",run_list)
    def test_run(self,case):
        print("----"*15)
        url = ConfigYaml().get_conf_url() + case[data_key.url]
        # print(type(case))#字典类型，dict
        # 映射
        case_id = case[data_key.case_id]
        case_model = case[data_key.case_model]
        case_name = case[data_key.case_name]
        pre_exec = case[data_key.pre_exec]
        method = case[data_key.method]
        params_type = case[data_key.params_type]
        params = case[data_key.params]
        expect_result = case[data_key.expect_result]
        headers = case[data_key.headers]
        cookies = case[data_key.cookies]
        code = case[data_key.code]
        db_verify = case[data_key.db_verify]
        db=case[data_key.db]
        db_Sql =[data_key.db_Sql]

        #替换匹配到的参数
        #data中参数替换

        head_info,data = self.replace_data(pre_exec=pre_exec, headers=headers, params=params)
        if cookies:
            head_info=cookies

        #数据格式化
        # header = json.loads(headers)#数据类型转换str->dict
        header = json.loads(head_info)
        if "json" in params_type:
            data =json.loads(data,strict=False)
        print("请求url:", url, "参数类型:", type(url))
        print("请求头：",header,"参数类型:",type(header))
        print("请求数据:",data,"参数类型:",type(data))

        #发送Request请求
        res=Request().requests_api(url=url,data=data,headers=header,method=str.lower(method))
        print("响应Code",res["code"],"\n""响应头：",res["headers"],"\n""响应体：",res["body"],"\n""响应数据类型:",type(res))

        # #获取token并写入
        if "login" in case_id:
            token = Base.re_find(str=str(res))
            with open('token.txt', 'a') as file_handle:
                file_handle.truncate(0)
                file_handle.write(str(token)+'\n')
                file_handle.close()

        print(res["code"],"8"*15)
        print(int(res["code"]))

        if db:
            import pymysql
            conn = pymysql.connect(
                host="172.16.34.149",
                user="zoedba",
                password="zoedba@)!^",
                # database='helloTest',
                charset="utf8",
                port=int("3306")
            )
            cursor = conn.cursor()
            sql = "select content from `base-msg`.`rec_sms_record` where receiver " \
                  "like '%13806069988%' and content like '%验证码%'order by create_time desc limit 1"
            cursor.execute(sql)
            fetchone = cursor.fetchone()
            print(fetchone)
            cursor.close()
            conn.close()
            # db_info = ConfigYaml().get_db_conf_info("db_1")
            # # 1、初始化数据库
            # mysql = Mysql(
            #     host=db_info["db_host"],
            #     user = db_info["db_user"],
            #     password = db_info["db_password"],
            #     database =db_info["db_name"],
            #     charset = db_info["db_charset"],
            #     port = int(db_info["db_port"])
            #
            # )
            # print(mysql.__dict__)
            # # sql = init_db("db_1")
            # # 2、查询sql，excel定义好的
            # db_res= mysql.fetchone(db_Sql)
            # print(db_res)
            # log.debug("数据库查询结果：{}".format(str(db_res)))
            # # # 3、数据库的结果与接口返回的结果验证
            # # # 获取数据库结果的key
            # # verify_list = list(dict(db_res).keys())
            # # # 根据key获取数据库结果，接口结果
            # # for line in verify_list:
            # #     res_line = res["body"][line]
            # #     res_db_line = dict(db_res)[line]

        #断言

        AssertUtil().assert_code(res["code"],code)
        AssertUtil().assert_in_body(str(res["body"]),str(expect_result))

        # allure
        # sheet名称  feature 一级标签
        allure.dynamic.feature(sheet_name)
        # 模块   story 二级标签
        allure.dynamic.story(case_model)
        # 用例ID+接口名称  title
        allure.dynamic.title(case_id + case_name)
        #用例级别
        allure.dynamic.severity(allure.severity_level.NORMAL)
        # 请求URL  请求类型 期望结果 实际结果描述
        desc = "<font color='red'>请求URL: </font> {}<Br/>" \
               "<font color='red'>请求类型: </font>{}<Br/>" \
               "<font color='red'>请求头: </font>{}<Br/>" \
               "<font color='red'>期望结果: </font>{}<Br/>" \
               "<font color='red'>实际结果: </font>{}".format(url, method,header, expect_result, res)
        allure.dynamic.description(desc)



    def replace_data(self,pre_exec=None, headers=None, params=None):
        """
        替换head和data中的内容
        :param pre_exec:
        :param headers:
        :param params:
        :return:
        """
        if pre_exec:
            token = self.replse()
            head_info = Base.res_sub(headers, token, pattern_data='\${(.*)}\$')
            data = Base.res_sub(params, token, pattern_data='\${(.*)}\$')
            return head_info, data
        else:
            return headers, params

    def replse(self):
        """
        token数据
        :return:token
        """
        with open('token.txt', 'r') as f:
            try:
                r = f.readlines()
                token = r[0].replace('tokenId=', '', 1).replace(';', '', 1).replace('\n','',1)#删除多余数据
                f.close()
                return token
            except IOError:
                return  "路径不正确"




if __name__ == '__main__':
    report_path = Conf.get_report_path() + os.sep + "result"
    report_html_path = Conf.get_report_path() + os.sep + "html"
    pytest.main(["-s", "Test_testLogin.py"])
    # sql = init_db("db_1")
    # 2、查询sql，excel定义好的
    # db_Sql = sql.fetchone("select content from `base-msg`.`rec_sms_record` where receiver like '%13806069988%' and content like '%验证码%'order by create_time desc limit 1")
    # db_res = sql.fetchone(db_Sql)
    # Base.allure_report("./report/result","./report/html")

