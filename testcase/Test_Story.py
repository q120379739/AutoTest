import json
import os
import allure

import pytest
import requests

from common import ExcelConfig, Base
from config import Conf
from config.Conf import ConfigYaml
from utils.LogUtil import my_log
from utils.RequestsUtil import Request
from  utils.AssertUtil import AssertUtil
from common.ExcelData import Data

#1、初始化信息
#1）.初始化测试用例文件
case_file = os.path.join(Conf.get_data_path(),ConfigYaml().get_excel_file())
#2）.测试用例sheet名称
sheet_name = ConfigYaml().get_excel_sheet()
#3）.获取运行测试用例列表
data_init = Data(case_file,sheet_name)
run_list = data_init.get_run_data()
#4）.日志
log = my_log()
#初始化dataconfig
data_key = ExcelConfig.DataConfig

# class


