# -*- coding:utf-8 -*-

import pymysql
import json

from config.Conf import ConfigYaml

db_info = ConfigYaml().get_db_conf_info("db_1")

class DBUtil:
    """mysql util"""
    db = None
    cursor = None
    def __init__(self):
            self.host=db_info["db_host"],
            self.user=db_info["db_user"],
            self.password=db_info["db_password"],
            self.database=db_info["db_name"],
            self.charset=db_info["db_charset"],
            self.port=int(db_info["db_port"])
            print("配置文件：" + json.dumps(db_info))

    # 链接数据库
    def get_con(self):
        """ 获取conn """
        self.db = pymysql.Connect(
            host=self.host,
            port=self.port,
            user=self.user,
            passwd=self.password,
            db=self.database,
            charset=self.charset
        )
        self.cursor = self.db.cursor()

    # 关闭链接
    def close(self):
        self.cursor.close()
        self.db.close()

    # 主键查询数据
    def get_one(self, sql):
        res = None
        try:
            self.get_con()
            self.cursor.execute(sql)
            res = self.cursor.fetchone()
            self.close()
        except Exception as e:
            print("查询失败！" + str(e))
        return res

    # 查询列表数据
    def get_all(self, sql):
        res = None
        try:
            self.get_con()
            self.cursor.execute(sql)
            res = self.cursor.fetchall()
            self.close()
        except Exception as e:
            print("查询失败！" + str(e))
        return res

    # 插入数据
    def __insert(self, sql):
        count = 0
        try:
            self.get_con()
            count = self.cursor.execute(sql)
            self.db.commit()
            self.close()
        except Exception as e:
            print("操作失败！" + str(e))
            self.db.rollback()
        return count

    # 保存数据
    def save(self, sql):
        return self.__insert(sql)

    # 更新数据
    def update(self, sql):
        return self.__insert(sql)

    # 删除数据
    def delete(self, sql):
        return self.__insert(sql)
if __name__ == '__main__':
    con = DBUtil()
    sql = "select content from `base-msg`.`rec_sms_record` where receiver " \
          "like '%13806069988%' and content like '%验证码%'order by create_time desc limit 1"
    con.get_con()
    print(con.get_one(sql))
    con.close()
