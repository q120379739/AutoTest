
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad



class AesCrypter(object):
    def __init__(self):
        self.key = bytes.fromhex('3132333435363738393041424344454631323334353637383930414243444566')#16进制字符转成byte数组
        self.iv = bytes.fromhex('30313233343536373839414243444546')
        self.mode = AES.MODE_CBC  # CBC 模式

    def encode(self, data):
        cipher = AES.new(self.key, self.mode, self.iv)  # 初始化一个AES
        pad_pkcs7 = pad(data.encode('utf-8'), AES.block_size, style='pkcs7')  #padding 是pkcs7
        result = cipher.encrypt(pad_pkcs7)  #加密结果是 bytes
        return result.hex() # 转化成16进制字符

    def decode(self, data):
        cipher = AES.new(self.key, self.mode, self.iv)
        una_pkcs7 = unpad(cipher.decrypt(bytes.fromhex(data)), AES.block_size, style='pkcs7')  # 将解密结果去除padding  返回 byte string : data without padding.
        decrypted_text = str(una_pkcs7, encoding='utf-8') # 将 byte string 转string
        return decrypted_text





if __name__ == '__main__':
    aes = AesCrypter()
    data1 = '13806069988'
    data2 = '6ce6210a63114e0eacca5a3cb56d7aa86c6c7f457da31905db8d17c38d4c24602e4f92920ff9b227b90a6fe88b96493b38d4708bb47a88a4e54f030213e4d2d1df827fba58d5c5c81bf65e308fc2bd4a8a25fce7d7f4400fe95a7abd77ce4f94e804ed9596cc29b452f8f2330626748827a6ea430f2b8ee0ce744bf6502ec770050425cf0b7c0c22cf467bcad6431ddbdc36c7e362ec304837ab01fd42ac541332b549a1deb08d83510b06a7798eb872c767cdf2d720a753aaf140b651393b65b86ae97c82981d6567e5a46d7e87d0e4cf3c4171204e46d7a58e876afad80f292c40c727ba4887056ff39d33fd7d77e0b1cbd92c72a8deb01094bdff0316460cc430e9525073c482cd5380739b4dd2d6fc2bcd3dce5f6ec13abf4b728d754674ed8193681d1bd42a6c82d88042ec7f2388ef79f88b285984e3496fe12b03885c3a22bba166cdbf80cd786653b99c1d2fb21d877e92f1ba741e0fe92464ff1d05ee7a50f93b49fe3385fdeb0015a2a6a92ea05b28b90da260c73a1697aa8621e73c1887d986f53b93347cc723ce096ae5a28159cc632a93db8c62ab5bc5aeccb6de9bf95840b21ea31375aacf9d905dceea471e3c229574cd6ea4602bf04553ce35fcc8d50252846acfb0f6cf0eaf5c7a58f3c4471f70d0b82442f9e54ec1b99da8588fb1c1f67e8549795c853332026b1f12bc432bd0783d4dbdb8c36c916f22e43943c9bb3b63f397e56f9987e05de58c3dea0ebcba194c2fed4f3dd2b374802414f1cd76c97470ce0c6461cda46c27c03835a93c02ce6025281fc5b9073dee799fb03bfaf1b7f382e4e413a62ada63d2089e8b0ba640cdf162e2c59fd629b735ccdd2439db1d9b17c4a7d16891bcff7fe431093623c7676541e9ab13fe3670b91f1785fe5deb29be522338ae7141457fd99ec3057d0ff8f14583162bd763c1a373da40c5a11bfc6946f1fa204cb21e4a4e95cbfa3d8e0f517951a56a030d074c56d3c5e1f771e96051fc492b9ac99cc5a1b1c3dd0775fbc2c2db8e575941ed'
    print('加密结果：', aes.encode(data1))
    print('解密结果：', aes.decode(data2))
